# chip8.js

A CHIP-8 virtual machine built in HTML & JavaScript without dependencies.

You can download this repository or check out the [live demo](https://chip8js.chrisledet.com/).

![screenshot](https://gitlab.com/chrisledet/chip8.js/raw/master/chip8.JPG)

## Download

Clone via git

```shell
$ git clone https://gitlab.com/chrisledet/chip8.js
```

Open `index.html` with your browser. The project comes with a bunch of cool ROMs inside the roms directory.

## TODOs

- Audio is currently broken due to no deps change
- There's one or two instructions that have not been

## License

Code is licensed under MIT. See the LICENSE file for details.

Each ROM were provided by www.zophar.net under public domain.
